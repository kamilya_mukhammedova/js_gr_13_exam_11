import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiItemData, Item } from '../models/item.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HttpClient) {
  }

  getItems() {
    return this.http.get<ApiItemData[]>(environment.apiUrl + '/items').pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(itemData => {
          return new Item(
            itemData._id,
            itemData.title,
            itemData.price,
            itemData.image,
            itemData.category,
            itemData.user
          );
        });
      })
    );
  }

  getItemsByCategories(categoryId: string) {
    return this.http.get<ApiItemData[]>(environment.apiUrl + `/items?category=${categoryId}`).pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(itemData => {
          return new Item(
            itemData._id,
            itemData.title,
            itemData.price,
            itemData.image,
            itemData.category,
            itemData.user
          );
        });
      })
    );
  }

}
