const mongoose = require('mongoose');
const {TITLES} = require("../constants");

const CategorySchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    enum: TITLES
  }
});

const Category = mongoose.model('Category', CategorySchema);
module.exports = Category;