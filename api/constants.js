const computers = 'computers';
const cars = 'cars';
const phones = 'phones';
const others = 'others';

module.exports = {
  computers,
  cars,
  phones,
  others,
  TITLES: [computers, cars, phones, others]
};