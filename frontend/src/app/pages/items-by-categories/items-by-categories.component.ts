import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { Item } from '../../models/item.model';
import { fetchItemsByCategoriesRequest } from '../../store/items.actions';

@Component({
  selector: 'app-items-by-categories',
  templateUrl: './items-by-categories.component.html',
  styleUrls: ['./items-by-categories.component.sass']
})
export class ItemsByCategoriesComponent implements OnInit {
  items: Observable<Item[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  categoryName: string = '';

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    this.items = store.select(state => state.items.items);
    this.loading = store.select(state => state.items.itemByCategoryLoading);
    this.error = store.select(state => state.items.itemByCategoryError);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const categoryId = params['id'];
      this.categoryName = params['title'];
      this.store.dispatch(fetchItemsByCategoriesRequest({categoryId}));
    });
  }

}
