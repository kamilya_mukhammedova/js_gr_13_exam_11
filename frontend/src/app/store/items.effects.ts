import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ItemsService } from '../services/items.service';
import {
  fetchItemsByCategoriesFailure,
  fetchItemsByCategoriesRequest, fetchItemsByCategoriesSuccess,
  fetchItemsFailure,
  fetchItemsRequest,
  fetchItemsSuccess
} from './items.actions';
import { catchError, mergeMap, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ItemsEffects {
  constructor(
    private actions: Actions,
    private itemsService: ItemsService
  ) {}

  fetchItems = createEffect(() => this.actions.pipe(
    ofType(fetchItemsRequest),
    mergeMap(() => this.itemsService.getItems().pipe(
      map(items => fetchItemsSuccess({items})),
      catchError(() => of(fetchItemsFailure({
        error: 'Something went wrong with items request!'
      })))
    ))
  ));

  fetchItemsByCategories = createEffect(() => this.actions.pipe(
    ofType(fetchItemsByCategoriesRequest),
    mergeMap(({categoryId}) => this.itemsService.getItemsByCategories(categoryId).pipe(
      map(items => fetchItemsByCategoriesSuccess({items})),
      catchError(() => of(fetchItemsByCategoriesFailure({
        error: 'Something went wrong with items by categories request!'
      })))
    ))
  ));

}
