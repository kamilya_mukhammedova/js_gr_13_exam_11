import { ItemsState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  fetchItemsByCategoriesFailure,
  fetchItemsByCategoriesRequest, fetchItemsByCategoriesSuccess,
  fetchItemsFailure,
  fetchItemsRequest,
  fetchItemsSuccess
} from './items.actions';

const initialState: ItemsState = {
  items: [],
  fetchLoading: false,
  fetchError: null,
  categoryId: null,
  itemByCategoryLoading: false,
  itemByCategoryError: null
};

export const itemsReducer = createReducer(
  initialState,
  on(fetchItemsRequest, state => ({...state, fetchLoading: true})),
  on(fetchItemsSuccess, (state, {items}) => ({
    ...state,
    fetchLoading: false,
    items
  })),
  on(fetchItemsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),

  on(fetchItemsByCategoriesRequest, (state, {categoryId}) => ({
    ...state,
    itemByCategoryLoading: true,
    categoryId
  })),
  on(fetchItemsByCategoriesSuccess, (state, {items}) => ({
    ...state,
    itemByCategoryLoading: false,
    items
  })),
  on(fetchItemsByCategoriesFailure, (state, {error}) => ({
    ...state,
    itemByCategoryLoading: false,
    itemByCategoryError: error
  })),
);
