const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const {nanoid} = require('nanoid');
const Category = require("./models/Category");
const {computers, cars, phones, others} = require("./constants");
const Item = require("./models/Item");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for(const coll of collections) {
    await mongoose.connection.dropCollection(coll.name);
  }

  const [user1, user2, user3] = await User.create({
    email: 'qwerty@com',
    password: '555',
    displayName: 'Jhon Doe',
    phoneNumber: '+996555111000',
    token: nanoid()
  }, {
    email: 'apple@com',
    password: '123',
    displayName: 'Alex Alex',
    phoneNumber: '+996550101010',
    token: nanoid()
  }, {
    email: 'banana@com',
    password: '999',
    displayName: 'Mike Swan',
    phoneNumber: '+996700909090',
    token: nanoid()
  });

  const [category1, category2, category3, category4] = await Category.create({
    title: computers
  }, {
    title: cars
  }, {
    title: phones
  }, {
    title: others
  });

  await Item.create({
    title: 'Samsung galaxy s21',
    description:
      'The Samsung Galaxy S21 series is the 2021 flagship family from the world’s biggest smartphone manufacturer.' +
      ' In 2020, Samsung tried to mostly appeal just to the ultra-premium buyer, but the 2021 Galaxy S21 series is a bit more well-rounded.',
    price: 300,
    image: 'samsung.jpg',
    category: category3,
    user: user1
  }, {
    title: 'Iphone XS max',
    description: '256 GG. The phone is in good condition. I used it carefully.',
    price: 200,
    image: 'iphone.jpg',
    category: category3,
    user: user2
  }, {
    title: 'Macbook Air',
    description: 'macOS Big Sur is fast and responsive, battery life is great, silent in use, keyboard remains very' +
      ' good',
    price: 700,
    image: 'macbook.jpg',
    category: category1,
    user: user2
  }, {
    title: 'Toyota Prius 2013',
    description: 'The 2013 Toyota Prius is an excellent choice in the compact car class. It\'s a gas-sipping hybrid that offers a winning mix of ride comfort, interior space, reliability, and high fuel economy. Factor in its good safety ratings and it\'s easy to see why we named it the 2013 Best Hatchback for Families',
    price: 10000,
    image: 'prius.jpg',
    category: category2,
    user: user3
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));