import { Item } from '../models/item.model';
import { Category } from '../models/category.model';

export type ItemsState = {
  items: Item[],
  fetchLoading: boolean,
  fetchError: null | string,
  categoryId: null | string,
  itemByCategoryLoading: boolean,
  itemByCategoryError: null | string,
};

export type CategoriesState = {
  categories: Category[],
  fetchLoading: boolean,
  fetchError: null | string,
};

export type AppState = {
  items: ItemsState,
  categories: CategoriesState
};
