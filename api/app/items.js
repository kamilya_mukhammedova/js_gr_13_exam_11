const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Item = require('../models/Item');
const mongoose = require("mongoose");
const auth = require("../middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    if(req.query.category) {
      const itemsInCategory = await Item.find({category: req.query.category});
      return res.send(itemsInCategory);
    }

    const items = await Item.find();
    return res.send(items);

  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const item = await Item.findById(req.params.id).populate('user', 'displayName phoneNumber');

    if(!item) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(item);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), auth, async (req, res, next) => {
  try {
    if (
      !req.body.title || !req.body.description ||
      !req.body.price || !req.body.image ||
      !req.body.category || !req.body.user
    ) {
      return res.status(400).send({message: 'All fields are required'});
    }

    const itemData = {
      title: req.body.title,
      description: req.body.description,
      price: req.body.price,
      image: req.file.filename,
      category: req.body.category,
      user: req.user._id
    };

    const newItem = new Item(itemData);

    await newItem.save();

    return res.send(newItem);
  } catch (error) {
    if(error instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(error);
    }
    return next(error);
  }
});

module.exports = router;