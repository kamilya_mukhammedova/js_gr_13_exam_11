import { createAction, props } from '@ngrx/store';
import { Item } from '../models/item.model';

export const fetchItemsRequest = createAction('[Items] Fetch Request');
export const fetchItemsSuccess = createAction(
  '[Items] Fetch Success',
  props<{ items: Item[] }>()
);
export const fetchItemsFailure = createAction(
  '[Items] Fetch Failure',
  props<{ error: string }>()
);

export const fetchItemsByCategoriesRequest = createAction(
  '[Items] Fetch Item in Category Request',
  props<{ categoryId: string }>()
);
export const fetchItemsByCategoriesSuccess = createAction(
  '[Items] Fetch Item in Category Success',
  props<{ items: Item[] }>()
);
export const fetchItemsByCategoriesFailure = createAction(
  '[Items] Fetch Item in Category Failure',
  props<{ error: string }>()
);
