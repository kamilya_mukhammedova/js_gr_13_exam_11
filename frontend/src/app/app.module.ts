import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { ItemsComponent } from './pages/items/items.component';
import { ImagePipe } from './pipes/image.pipe';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { itemsReducer } from './store/items.reducer';
import { ItemsEffects } from './store/items.effects';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { categoriesReducer } from './store/categories.reducer';
import { CategoriesEffects } from './store/categories.effects';
import { ItemsByCategoriesComponent } from './pages/items-by-categories/items-by-categories.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    ItemsComponent,
    ImagePipe,
    ItemDetailsComponent,
    ItemsByCategoriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    MatMenuModule,
    StoreModule.forRoot({items: itemsReducer, categories: categoriesReducer}, {}),
    EffectsModule.forRoot([ItemsEffects, CategoriesEffects]),
    MatProgressSpinnerModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
