import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemsComponent } from './pages/items/items.component';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { ItemsByCategoriesComponent } from './pages/items-by-categories/items-by-categories.component';

const routes: Routes = [
  {path: '', component: ItemsComponent},
  {path: 'items/:id', component: ItemDetailsComponent},
  {path: 'categories/:id/:title', component: ItemsByCategoriesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
