import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { Item } from '../../models/item.model';
import { fetchItemsRequest } from '../../store/items.actions';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.sass']
})
export class ItemsComponent implements OnInit {
  items: Observable<Item[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.items = store.select(state => state.items.items);
    this.loading = store.select(state => state.items.fetchLoading);
    this.error = store.select(state => state.items.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchItemsRequest());
  }
}
