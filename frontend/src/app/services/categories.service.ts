import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiCategoryData, Category } from '../models/category.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get<ApiCategoryData[]>(environment.apiUrl + '/categories').pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(categoryData => {
          return new Category(
            categoryData._id,
            categoryData.title,
          );
        });
      })
    )
  }

}
