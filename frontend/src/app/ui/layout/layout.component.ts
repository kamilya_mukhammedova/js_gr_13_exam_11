import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Category } from '../../models/category.model';
import { fetchCategoriesRequest } from '../../store/categories.actions';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  categories: Observable<Category[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>
  ) {
    this.categories = store.select(state => state.categories.categories);
    this.loading = store.select(state => state.categories.fetchLoading);
    this.error = store.select(state => state.categories.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCategoriesRequest());
  }

}
